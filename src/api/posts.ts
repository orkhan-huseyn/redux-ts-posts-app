import axios from './axios';
import { Post } from '../types';

export async function getPosts(): Promise<Post[]> {
  const { data } = await axios.get('posts');
  return data;
}

export async function deletePost(id: number): Promise<void> {
  await axios.delete('posts/' + id);
}

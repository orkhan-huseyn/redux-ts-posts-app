import * as postsServices from './posts';

export default {
  ...postsServices,
};

import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPosts, removePost } from './features/postsSlice';
import { AppDispatch, RootState } from './store';

function App() {
  const postsState = useSelector((state: RootState) => state.posts);
  const dispatch: AppDispatch = useDispatch();

  const { posts, loading, error } = postsState;

  useEffect(() => {
    dispatch(fetchPosts());
  }, []);

  function handleDelete(postId: number) {
    dispatch(removePost(postId));
  }

  return (
    <div className="container">
      <h1 className="text-center">Posts App</h1>
      <div className="list-group mt-3">
        {loading && (
          <div className="d-flex justify-content-center">
            <div className="spinner-border text-primary" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        )}
        {error && (
          <div className="alert alert-danger" role="alert">
            {error.message || 'Oops! Something went wrong...'}
          </div>
        )}
        {posts.map((post) => (
          <div
            key={post.id}
            onClick={() => handleDelete(post.id)}
            className="list-group-item list-group-item-action"
          >
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">{post.title}</h5>
              <small>3 days ago</small>
            </div>
            <p className="mb-1">{post.body}</p>
            <small>And some small print.</small>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;

import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import API from '../api';
import { Post } from '../types';

type AppError = {
  message: string | undefined;
};

type PostsState = {
  posts: Post[];
  loading: boolean;
  error: AppError | null;
};

export const fetchPosts = createAsyncThunk(
  'posts/fetchPosts',
  async function () {
    const posts = await API.getPosts();
    return posts;
  }
);

export const removePost = createAsyncThunk(
  'posts/deletePost',
  async function (id: number, { dispatch }) {
    await API.deletePost(id);
    dispatch(deletePostFromStore(id));
  }
);

const initialState: PostsState = {
  posts: [],
  loading: true,
  error: null,
};

const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    deletePostFromStore(state, action) {
      state.posts = state.posts.filter((post) => post.id !== action.payload);
    },
  },
  extraReducers(builder) {
    builder.addCase(fetchPosts.fulfilled, (state, action) => {
      state.loading = false;
      state.posts = action.payload;
    });

    builder.addCase(fetchPosts.rejected, (state, action) => {
      state.error = {
        message: action.error.message,
      };
      state.loading = false;
    });

    builder.addCase(removePost.pending, (state, action) => {
      state.loading = true;
    });

    builder.addCase(removePost.fulfilled, (state, action) => {
      state.loading = false;
    });
  },
});

export const { deletePostFromStore } = postsSlice.actions;

export default postsSlice.reducer;
